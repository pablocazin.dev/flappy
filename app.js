let flappy = document.getElementById('flappy');
let bloc1 = document.getElementById('bloc1');
let bloc2 = document.getElementById('bloc2');
let score = document.getElementById('score');

/* position de départ de flappy */
let flappyY = 400;
flappy.style.top = flappyY + "px";

var interval = window.setInterval(() => {
    gravity(),
    blocMovement(),
    scoreDisplay()
}, 1);



let gravityValue = 1.5;
function gravity() {
    if(flappy.offsetTop > 650) {
        gravityValue = 0;
    }
    gravityValue = gravityValue*1.02;
    flappyY += gravityValue;
    flappy.style.top = flappyY + "px";
}

let jumpValue = 100;
document.addEventListener('keydown', function (event) {
    if(event.code === 'Space') {
        flappyY -= jumpValue;
        flappy.style.top = flappyY + "px";
        gravityValue = 0;
        setTimeout(() => gravityValue = 1, 250)
    }
})

function randomIntFromInterval(min, max) { 
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

let blocX = 1200;
let blocValue = 1;
let bloc1_height = 200;
let bloc2_height = 200;
bloc1.style.height = bloc1_height + "px";
bloc2.style.height = bloc2_height + "px";
let between_space = 200;

function blocMovement() {

    if(
       (flappy.offsetTop <= bloc1_height || (flappy.offsetTop + 50) >= (bloc1_height + between_space)) &&
       ((flappy.offsetLeft + 50) >= bloc1.offsetLeft && (flappy.offsetLeft + 50) <= bloc1.offsetLeft + 50) ||
       (flappy.offsetTop + 50) >= 700
    ) 
    {
        blocValue = 0;
        jumpValue = 0;
    }
    blocX -= blocValue;
    bloc1.style.left = blocX + "px";
    bloc2.style.left = blocX + "px";

    if(bloc1.offsetLeft <= -80) {
        blocX = 880;
    
        bloc1_height = randomIntFromInterval(100, 500);
        bloc2_height = 700 - between_space - bloc1_height;
    
        bloc1.style.height = bloc1_height + "px";
        bloc2.style.height = bloc2_height + "px";
    }
}

function scoreDisplay() {
    let scoreCount = 0;
    score.innerHTML = scoreCount;
    scoreCount++

    if(bloc1.offsetLeft <= 200){
        scoreCount++
    }
}







